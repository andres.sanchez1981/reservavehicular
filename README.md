# xalok_wsanchez

## Name
Sistema de Reservas

## Description
Sistema para reserva de vehiculos y conductores.
Las reglas especificadas en el requerimiento fueron incluidas y se agregó el concepto de que un vehiculo que no necesita licencia puede tener un conductor con o sin licencia.
La base de datos utilizada fue Postgresql 12, durante el desarrollo se hicieron consultas directo a las tablas y se hizo uso de una vista para poder consultar los vehiculos y los conductores que podian hacer uso de dicho vehiculo basandose en la regla de licencias explicada previamente.
El sistema fue desarrollado en PHP 7.4 con el framework CodeIgniter 3, este fue modificado para usar la arquitectura HMVC, por lo que dentro de application/modules van a encontrar carpetas de los modulos con la estructura controller, models, views.
Este framework es la base de multiples proyectos que he desarrollado por lo que se usan metodos que abstraen muchas funcionalidades repetittivas, por ejemplo las llamadas ajax y la carga de los formularios de edición de objetos.

## Installation
Una vez clonado el repositorio se debe crear la base de datos haciendo uso del archivo /database/database.sql, modificar los archivos database.php y config.php para poner las credenciales y rutas correctas.

## Authors and acknowledgment
Washington Sánchez

## License
For open source projects, say how it is licensed.

## Project status
