--
-- PostgreSQL database dump
--

-- Dumped from database version 12.12
-- Dumped by pg_dump version 12.12

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: logistic; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA logistic;


ALTER SCHEMA logistic OWNER TO postgres;

--
-- Name: resources; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA resources;


ALTER SCHEMA resources OWNER TO postgres;

--
-- Name: trip_seq; Type: SEQUENCE; Schema: logistic; Owner: postgres
--

CREATE SEQUENCE logistic.trip_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE logistic.trip_seq OWNER TO postgres;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: trip; Type: TABLE; Schema: logistic; Owner: postgres
--

CREATE TABLE logistic.trip (
    id integer DEFAULT nextval('logistic.trip_seq'::regclass) NOT NULL,
    vehicle_id integer NOT NULL,
    driver_id integer NOT NULL,
    date date NOT NULL,
    estado character varying(1) NOT NULL
);


ALTER TABLE logistic.trip OWNER TO postgres;

--
-- Name: COLUMN trip.estado; Type: COMMENT; Schema: logistic; Owner: postgres
--

COMMENT ON COLUMN logistic.trip.estado IS 'A Active I inactive';


--
-- Name: driver_seq; Type: SEQUENCE; Schema: resources; Owner: postgres
--

CREATE SEQUENCE resources.driver_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE resources.driver_seq OWNER TO postgres;

--
-- Name: driver; Type: TABLE; Schema: resources; Owner: postgres
--

CREATE TABLE resources.driver (
    id integer DEFAULT nextval('resources.driver_seq'::regclass) NOT NULL,
    name character varying(255) NOT NULL,
    surname character varying(255) NOT NULL,
    license character varying(1) NOT NULL,
    estado character varying(1) NOT NULL
);


ALTER TABLE resources.driver OWNER TO postgres;

--
-- Name: COLUMN driver.license; Type: COMMENT; Schema: resources; Owner: postgres
--

COMMENT ON COLUMN resources.driver.license IS 'Y o N';


--
-- Name: COLUMN driver.estado; Type: COMMENT; Schema: resources; Owner: postgres
--

COMMENT ON COLUMN resources.driver.estado IS 'A Active I inactive';


--
-- Name: vehicle_seq; Type: SEQUENCE; Schema: resources; Owner: postgres
--

CREATE SEQUENCE resources.vehicle_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE resources.vehicle_seq OWNER TO postgres;

--
-- Name: vehicle; Type: TABLE; Schema: resources; Owner: postgres
--

CREATE TABLE resources.vehicle (
    id integer DEFAULT nextval('resources.vehicle_seq'::regclass) NOT NULL,
    brand character varying(255) NOT NULL,
    model character varying(255) NOT NULL,
    plate character varying(10) NOT NULL,
    license_required character varying(1) NOT NULL,
    estado character varying(1) NOT NULL
);


ALTER TABLE resources.vehicle OWNER TO postgres;

--
-- Name: COLUMN vehicle.license_required; Type: COMMENT; Schema: resources; Owner: postgres
--

COMMENT ON COLUMN resources.vehicle.license_required IS 'Y o N';


--
-- Name: COLUMN vehicle.estado; Type: COMMENT; Schema: resources; Owner: postgres
--

COMMENT ON COLUMN resources.vehicle.estado IS 'A Active I inactive';


--
-- Name: v_vehicle_driver_combination; Type: VIEW; Schema: resources; Owner: postgres
--

CREATE VIEW resources.v_vehicle_driver_combination AS
 SELECT v.id AS vehicle_id,
    d.id AS driver_id,
        CASE
            WHEN (((v.license_required)::text = 'Y'::text) AND ((d.license)::text = 'Y'::text)) THEN 1
            WHEN (((v.license_required)::text = 'Y'::text) AND ((d.license)::text = 'N'::text)) THEN 0
            WHEN ((v.license_required)::text = 'N'::text) THEN 1
            ELSE NULL::integer
        END AS valido
   FROM (resources.vehicle v
     LEFT JOIN resources.driver d ON (((d.estado)::text = 'A'::text)))
  WHERE ((v.estado)::text = 'A'::text);


ALTER TABLE resources.v_vehicle_driver_combination OWNER TO postgres;

--
-- Data for Name: trip; Type: TABLE DATA; Schema: logistic; Owner: postgres
--

COPY logistic.trip (id, vehicle_id, driver_id, date, estado) FROM stdin;
3	2	1	2024-02-08	A
2	3	4	2024-02-08	I
1	1	4	2024-02-08	A
\.


--
-- Data for Name: driver; Type: TABLE DATA; Schema: resources; Owner: postgres
--

COPY resources.driver (id, name, surname, license, estado) FROM stdin;
1	Washington	Sanchez	Y	A
3	Cristiano	Ronaldo	Y	A
4	Erling	Haaland	Y	A
2	Lionel	Messi	N	I
\.


--
-- Data for Name: vehicle; Type: TABLE DATA; Schema: resources; Owner: postgres
--

COPY resources.vehicle (id, brand, model, plate, license_required, estado) FROM stdin;
1	BMW	Serie 4 Coupé	AAA-001	Y	A
3	Mercedes	EQB SUV	AAA-003	N	A
2	Ferrari	296 GTS	AAA-002	Y	I
\.


--
-- Name: trip_seq; Type: SEQUENCE SET; Schema: logistic; Owner: postgres
--

SELECT pg_catalog.setval('logistic.trip_seq', 3, true);


--
-- Name: driver_seq; Type: SEQUENCE SET; Schema: resources; Owner: postgres
--

SELECT pg_catalog.setval('resources.driver_seq', 5, true);


--
-- Name: vehicle_seq; Type: SEQUENCE SET; Schema: resources; Owner: postgres
--

SELECT pg_catalog.setval('resources.vehicle_seq', 3, true);


--
-- Name: trip trip_pkey; Type: CONSTRAINT; Schema: logistic; Owner: postgres
--

ALTER TABLE ONLY logistic.trip
    ADD CONSTRAINT trip_pkey PRIMARY KEY (id);


--
-- Name: driver driver_pkey; Type: CONSTRAINT; Schema: resources; Owner: postgres
--

ALTER TABLE ONLY resources.driver
    ADD CONSTRAINT driver_pkey PRIMARY KEY (id);


--
-- Name: vehicle vehicle_pkey; Type: CONSTRAINT; Schema: resources; Owner: postgres
--

ALTER TABLE ONLY resources.vehicle
    ADD CONSTRAINT vehicle_pkey PRIMARY KEY (id);


--
-- PostgreSQL database dump complete
--

