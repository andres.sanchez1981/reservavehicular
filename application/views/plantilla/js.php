
<script src="<?= base_url() . "assets/" ?>bootstrap4/js/jquery-ui.min.js"></script>
<script src="<?= base_url() . "assets/" ?>bootstrap4/js/pooper.min.js"></script>
<script src="<?= base_url() . "assets/" ?>bootstrap4/js/moment.min.js"></script>
<script src="<?= base_url() . "assets/" ?>bootstrap4/plugins/daterangepicker/js/daterangepicker.min.js"></script>

<script src="<?= base_url() . "assets/" ?>adminlte3/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<script src="<?= base_url() . "assets/" ?>bootstrap4/js/bootstrap.bundle.min.js"></script>
<script src="<?= base_url() . "assets/" ?>adminlte3/plugins/toastr/toastr.min.js"></script>

<script src="<?= base_url() . "assets/" ?>adminlte3/plugins/select2/js/select2.full.min.js"></script>
<script src="<?= base_url() . "assets/" ?>adminlte3/plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js"></script>
<script src="<?= base_url() . "assets/" ?>adminlte3/plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="<?= base_url() . "assets/" ?>adminlte3/plugins/jquery-validation/additional-methods.min.js"></script>

<script src="<?= base_url() . "assets/" ?>ckeditor_s/ckeditor.js"></script>
<script src="<?= base_url() . "assets/" ?>ckeditor_s/adapters/jquery.js"></script>

<script src="<?= base_url() . "assets/" ?>adminlte3/plugins/sweetalert2/sweetalert2download.js"></script>

<script src="<?= base_url() . "assets/" ?>adminlte3/dist/js/adminlte.min.js"></script>

<script src="<?= base_url() . "assets/" ?>adminlte3/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?= base_url() . "assets/" ?>adminlte3/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?= base_url() . "assets/" ?>adminlte3/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?= base_url() . "assets/" ?>adminlte3/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="<?= base_url() . "assets/" ?>adminlte3/plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?= base_url() . "assets/" ?>adminlte3/plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="<?= base_url() . "assets/" ?>adminlte3/plugins/jszip/jszip.min.js"></script>
<script src="<?= base_url() . "assets/" ?>adminlte3/plugins/pdfmake/pdfmake.min.js"></script>
<script src="<?= base_url() . "assets/" ?>adminlte3/plugins/pdfmake/vfs_fonts.js"></script>
<script src="<?= base_url() . "assets/" ?>adminlte3/plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="<?= base_url() . "assets/" ?>adminlte3/plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="<?= base_url() . "assets/" ?>adminlte3/plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
<script>
    var session = <?= (isset($this->session->userFincaId))? $this->session->userFincaId:0 ?>;
    if (session == 1) {
        $('.solo_rosaholics').css('display', 'inline-block');
    }

    $(document).ready(function () {
        setTimeout(validaciones_buttons, 1);
        $('#fincas_selecionadas').on('select2:select', function (e) {
            var selectedValue = e.params.data.id;
            if (selectedValue === '0') {
                $('#fincas_selecionadas').val('0').trigger('change');
            } else {
                var selectedOptions = $('#fincas_selecionadas').val() || [];
                var indexZero = selectedOptions.indexOf('0');
                if (indexZero !== -1) {
                    selectedOptions.splice(indexZero, 1);
                    $('#fincas_selecionadas').val(selectedOptions).trigger('change');
                }
            }
        });
    });
    function validaciones_buttons() {
        if ($('select').hasClass('no_editable') || $('button').hasClass('no_editable') || $('div').hasClass('no_editable')) {
            $(".no_editable").addClass('disabled');
            $(".no_editable").click(function () {
                return false;
            });
            $('.no_editable').prop('disabled', true);
            $(".no_editable :input").attr("disabled", "disabled");
        }
    }
    var loadingBtn = "<div class='spinner-border spinner-border-sm' role='status'><span class='sr-only'>Loading...</span></div>";
    
    $(document).on('click', '#btn_subir_archivo', function (e) {
        $(this).val("presionado");
        $("#btn_subir_archivo").html(loadingBtn);
        $("#btn_subir_archivo").unbind('click');
    });
    $(document).on('click', '#btn_buscar', function (e) {
        if (typeof validar_submit_pagina !== 'undefined') {
            console.log("Hay que validar_submit_pagina");
            if (!validar_submit_pagina()) {
                console.log("No Validado");
                return false;
            }
            console.log("Validado");
        }


        $(this).val("presionado");
        $("#btn_buscar").html(loadingBtn);
        $("#btn_buscar").unbind('click');
    });
    function llamadaAjax(idBoton, url, parametros, callback) {
//        console.log("Llamada Ajax "+idBoton+url+parametros+callback);
        if (idBoton) {
            var textoBtn = $("#" + idBoton).html();
            console.log("Tomo el boton");
            $("#" + idBoton).html(loadingBtn);
            console.log("cambio a loading");
            $("#" + idBoton).attr('disabled', true);
        }
        $.ajax({
            url: url,
            type: 'post',
            data: parametros,
            cache: false,
            success: function (r) {
                if (idBoton) {
                    console.log("Tomo el boton de vuelta");
                    $("#" + idBoton).html(textoBtn);
                    $("#" + idBoton).attr('disabled', false);
                    console.log("habilito el boton");
                }
                if (callback !== 0) {
                    eval(callback)(r);
                }
            }
        });
    }
    //Modal
    function swal_info(_mensaje)
    {
        Swal.fire(
                'Información',
                _mensaje,
                'info');
    }

    function swal_listo(_mensaje)
    {
        Swal.fire({
            position: 'top-end',
            icon: 'success',
            title: _mensaje,
            showConfirmButton: false,
            timer: 1500
        });
    }

    function swal_error(_mensaje)
    {
        Swal.fire({
            position: 'top-end',
            icon: 'error',
            title: _mensaje,
            showConfirmButton: false,
            timer: 1500
        });
    }

    function swal_modal(texto, leyenda_afirmativa, leyenda_denegacion, url, parametros, callback)
    {
        var respuesta = false;
        Swal.fire({
            title: texto,
            icon: 'info',
            showConfirmButton: true,
            showDenyButton: true,
            showCancelButton: false,
            confirmButtonText: leyenda_afirmativa,
            denyButtonText: leyenda_denegacion
        }).then((result) => {
            /* Read more about isConfirmed, isDenied below */
            if (result.isConfirmed) {
                llamadaAjax(false, url, parametros, callback);
                // swal_listo('Registro inactivado');
            } else if (result.isDenied) {
                //Evento que se hará cuando se denegara
            }
        });
    }

    function btnLlamadaAjax(idBtn, url, funcionPrevia, callback) {
        $('#' + idBtn).on("click", function () {
            var parametros = eval(funcionPrevia)();
            console.log(idBtn + "  " + parametros);
            llamadaAjax(idBtn, url, parametros, callback);
        });
    }
function dataToSelect(idSelect, data) {
        $.each(data, function (key, obj) {
            $(idSelect).attr('disabled', false);
            var option = document.createElement('option');
            option.appendChild(document.createTextNode(obj.valor));
            option.value = obj.clave;
            var datos = obj.arr_data;
            if (datos != undefined) {
                if (Object.keys(datos).length > 0) {
                    for (key in datos) {
                        if (datos.hasOwnProperty(key)) {
                            var value = datos[key];
                            option.dataset.nombre = value;
                        }
                    }
                }
            }
            $(idSelect).append(option);
        });
        $(idSelect).select2();
    }
    function llenarSelect(idSelect, url, parametros, callback = false) {
        $("#" + idSelect).html("Loading....");
        $("#" + idSelect).attr('disabled', true);
        $.ajax({
            url: url,
            type: 'post',
            data: parametros,
            cache: false,
            success: function (r) {
                $("#" + idSelect).html("");
                dataToSelect("#" + idSelect, r);
                if (callback) {
                    eval(callback)();
                }
            }
        });
    }

    function enlazarSelect(idSelOrigen, idSelDestino, url, callback) {
        console.log("enlazarSelect");
        $('#' + idSelOrigen).on("change", function () {
            var data = {
                "id": $('#' + idSelOrigen).val()
            };
            llenarSelect(idSelDestino, url, data, callback);
        });
    }
</script>

<script>
    (function ($) {
        $.fn.inputFilter = function (inputFilter) {
            return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function () {
                if (inputFilter(this.value)) {
                    this.oldValue = this.value;
                    this.oldSelectionStart = this.selectionStart;
                    this.oldSelectionEnd = this.selectionEnd;
                } else if (this.hasOwnProperty("oldValue")) {
                    this.value = this.oldValue;
                    this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
                } else {
                    this.value = "";
                }
            });
        };
    }(jQuery));
    function aplicarSoloNumeros() {
        $(".soloNumeros").inputFilter(function (value) {
            return /^-?\d*$/.test(value);
        });
    }
    function aplicarSoloNumerosDecimales() {
        $(".soloNumerosDecimales").inputFilter(function (value) {
//            return /^-?\d*$/.test(value);
            return /^-?\d*[.]?\d*$/.test(value);
        });
    }
    aplicarSoloNumeros();
    aplicarSoloNumerosDecimales();
    /*
     // Install input filters.
     //Integer
     $("#intTextBox").inputFilter(function(value) {
     return /^-?\d*$/.test(value); });
     //Integer > = 0
     $("#uintTextBox").inputFilter(function(value) {
     return /^\d*$/.test(value); });
     //Integer >= 0 and <= 500
     $("#intLimitTextBox").inputFilter(function(value) {
     return /^\d*$/.test(value) && (value === "" || parseInt(value) <= 500); });
     //Float (use . or , as decimal separator)
     $("#floatTextBox").inputFilter(function(value) {
     return /^-?\d*[.,]?\d*$/.test(value); });
     //Currency (at most two decimal places)
     $("#currencyTextBox").inputFilter(function(value) {
     return /^-?\d*[.,]?\d{0,2}$/.test(value); });
     //A-Z only
     $("#latinTextBox").inputFilter(function(value) {
     return /^[a-z]*$/i.test(value); });
     //Hexadecimal
     $("#hexTextBox").inputFilter(function(value) {
     return /^[0-9a-f]*$/i.test(value); });
     */

    function mostrarError($mensaje) {
        toastr.error($mensaje);
    }
    function mostrarExito($mensaje) {
        toastr.success($mensaje);
    }


    function actualizarTotalesResumen(store_id, rango_busqueda, tipo_calendario, tipo) {
        console.log("actualizarTotalesResumen");
        $(".totales_resumen").html('ali');

        $.ajax({
            type: "POST",
            url: "<?= base_url() ?>/ecommerce/totalResumen",
            cache: false,
            data: {store_id: store_id, rango_busqueda: rango_busqueda, tipo_calendario: tipo_calendario, tipo: tipo}, // serializes the form's elements.
            success: function (data)
            {
                console.log(data);
                if (data.error) {
                    mostrarError(data.respuesta);
                } else {
                    console.log("actualizo total");
                    $(".totales_resumen").html(data.respuesta);
                }
            }
        });

    }



    $(document).ready(function () {

        $('body').on('hidden.bs.modal', function () {
            if ($('.modal.show').length > 0)
            {
                $('body').addClass('modal-open');
            }
        });

        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        });


        $('.select2').select2({
//            minimumInputLength: 3
        });
        $('.fecha_rango').daterangepicker({
            locale: {
                format: '<?= FORMATO_FECHA_DATEPICKER_JS ?>'
            },
            autoApply: true
        });
        $('.fecha_unica').daterangepicker({
            singleDatePicker: true,
            locale: {
                format: '<?= FORMATO_FECHA_DATEPICKER_JS ?>'
            },
            autoApply: true
        });
        $('.fecha_rango_full').daterangepicker({
            timePicker: true,
            timePickerSeconds: true,
            locale: {
                format: '<?= FORMATO_FECHA_DATEPICKER_FULL_JS ?>'
            },
            autoApply: true
        });
        $('.select2bs4').select2({
            theme: 'bootstrap4'
        })


        /*para la paginacion*/
        $(".item_paginacion").on("click", function (e) {
            e.preventDefault();
            if ($(this).text() != '...') {
                $("#paginacion_pag").val($(this).text());
                $("#btn_buscar").trigger("click");
            }
        });
        $("#order_number").on('keypress', function (e) {
            if (e.which === 13) {
                $("#btn_buscar").trigger("click");
            }
        });
        $('#myModal').on('shown.bs.modal', function () {
            $('#myModal').trigger('focus');
        });

        $(document).on("click", ".togglePassword", function () {
            if ($(this).prev().prop("type") == "password") {
                $(this).prev().prop("type", "text");
                $(this).find("i").removeClass('fa-eye-slash').addClass('fa-eye');
            } else {
                $(this).prev().prop("type", "password");
                $(this).find("i").removeClass('fa-eye').addClass('fa-eye-slash');
            }
        });
        /*** toogle password visibility ***/

    });


    var deshabilitarClick = 0;
    function unsoloclick(clase = false) {
        deshabilitarClick++;
        console.log("deshabilitarClick " + deshabilitarClick);
        if (deshabilitarClick === 1) {
//        if (!$(clase).prop('disabled')) {
            if (clase) {
                $(clase).prop('disabled', true);
            }
            setTimeout(function () {
                console.log('quitamos lo deshabilitado js');
                if (clase) {
                    $(clase).removeAttr('disabled');
                }
                deshabilitarClick = 0;
            }, 1000);
            return true;
        } else {
            console.log("Se controla un clic innecesario");
        }
        return false;
    }

<?php
if (isset($error)) {
    if (is_array($error)) {
        foreach ($error as $err) {
            echo "mostrarError('" . htmlspecialchars(addslashes($err)) . "');";
        }
    } else {
        echo "mostrarError('" . $error . "');";
    }
}

if (isset($exito)) {
    if (is_array($exito)) {
        foreach ($exito as $xt) {
            echo "mostrarExito('" . htmlspecialchars(addslashes($xt)) . "');";
        }
    } else {
        echo "mostrarExito('" . $exito . "');";
    }
}
?>


    function mostrarOcultarPassword() {
        var x = document.getElementById("myInput");
        if (x.type === "password") {
            x.type = "text";
        } else {
            x.type = "password";
        }
    }

    function mostrarEdicionTopMenu(r) {
        if (r.error) {
            mostrarError("No existe información disponible en estos momentos");
        } else {
            $("#modalEdicionTop .modal-content").html(r.respuesta);
            $("#modalEdicionTop").modal("show");
        }
    }

    var charts = [];
    function preparar_data(data, type_chart) {
        let datasets_obj = Object.keys(data[0]);
        var arr_obj = [];
        if (type_chart == 'bar_stacked') {
            type_chart = 'bar';
        }
        datasets_obj.forEach(item => {
            if (item.includes('val_')) {
                if (item != 'indice') {
                    let item_array = item.split('_');
                    var label = '';
                    item_array.forEach(item_2 => {
                        if (item_2 != 'val') {
                            label = label + ' ' + item_2.toUpperCase();
                        }
                    });
                    var linea_obj = {
                        type: type_chart,
                        label: label,
                        data: data,
                        parsing: {
                            yAxisKey: item,
                            xAxisKey: 'indice',
                            key: item
                        },
                        tension: 0.5
                    }
                    arr_obj.push(linea_obj);
                }
            }
        });
        return arr_obj;
    }

    function graficar(grafico_id, datasets, type_chart, labels, update) {
        var id_chart = grafico_id.canvas.id;
        var data = {
            datasets: datasets
        }
        var options = {};
        if (type_chart == 'pie' || type_chart == 'doughnut') {
            data.labels = labels;
        } else if (type_chart == 'bar_stacked') {
            options = {
                plugins: {
                    zoom: {
                        zoom: {
                            wheel: {
                                enabled: true,
                            },
                            pinch: {
                                enabled: true
                            },
                            mode: 'y',
                        },
                        limits: {
                            y: {min: 0},
                            y2: {min: 0}
                        },
                    }
                },
                responsive: true,
                scales: {
                    x: {
                        stacked: true
                    },
                    y: {
                        beginAtZero: true
                    }
                }
            }
        } else {

            options = {
                plugins: {
                    zoom: {
                        zoom: {
                            wheel: {
                                enabled: true,
                            },
                            pinch: {
                                enabled: true
                            },
                            mode: 'y',
                        },
                        limits: {
                            y: {min: 0},
                            y2: {min: 0}
                        },
                    }
                },
                responsive: true,
                scales: {
                    y: {
                        beginAtZero: true
                    }
                }

            }
        }

        if (update) {
            charts[id_chart].config.data = data;
            charts[id_chart].config.options = options;
            charts[id_chart].update();
        } else {
            charts[id_chart] = new Chart(grafico_id, {
                data: data,
                options: options
            });
        }

    }

    $(document).ready(function () {
        /*************** ACCIONES MENU TOP *********************/
        $(".btn-accion").on('click', function () {
            if ($(this).val() === "menu_top_password") {
                unsoloclick('.btn-accion');
                llamadaAjax(false, '<?= base_url() ?>seguridad/usuario/editar_password', {
                    "id": $(this).data('id')
                }, mostrarEdicionTopMenu);
            }
        });

        $(document).on('click', '.copyurlorder', function () {
            var text = $(this).data('url');
            var order_id = $(this).data('order_id');
            navigator.clipboard.writeText(text);
            $('.mensaje_copy_' + order_id).html('copied!');
            console.log('dentro de la funcion');
            setTimeout(function () {
                $('.mensaje_copy_' + order_id).html('Copy link');
            }, 1000);
        });
    });

    function analizarRespuesta(r) {
        if (r.error) {
            mostrarError(r.mensaje);
            return false;
        } else {
            mostrarExito(r.mensaje);
            return true;
        }
        return false;
    }
</script>

