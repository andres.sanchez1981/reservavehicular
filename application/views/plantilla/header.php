<link rel = "stylesheet" href = "<?= base_url() . "assets/" ?>adminlte3/plugins/fontawesome-free/css/all.min.css">
<script src="https://kit.fontawesome.com/458553bd15.js" crossorigin="anonymous"></script>
<link rel = "stylesheet" href = "<?= base_url() . "assets/" ?>bootstrap4/css/bootstrap.min.css">
<link rel = "stylesheet" href = "<?= base_url() . "assets/" ?>adminlte3/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
<link rel = "stylesheet" href = "<?= base_url() . "assets/" ?>adminlte3/plugins/jqvmap/jqvmap.min.css">

<link rel = "stylesheet" href = "<?= base_url() . "assets/" ?>adminlte3/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
<link rel = "stylesheet" href = "<?= base_url() . "assets/" ?>/bootstrap4/plugins/daterangepicker/css/daterangepicker.css" />
<link rel = "stylesheet" href = "<?= base_url() . "assets/" ?>adminlte3/plugins/select2/css/select2.min.css">
<link rel = "stylesheet" href = "<?= base_url() . "assets/" ?>adminlte3/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
<link rel = "stylesheet" href = "<?= base_url() . "assets/" ?>adminlte3/plugins/bootstrap4-duallistbox/bootstrap-duallistbox.css">

<link rel = "stylesheet" href = "<?= base_url() . "assets/" ?>adminlte3/dist/css/adminlte.min.css">

<link rel = "stylesheet" href = "<?= base_url() . "assets/" ?>adminlte3/plugins/summernote/summernote-bs4.css">
<link rel = "stylesheet" href = "<?= base_url() . "assets/" ?>adminlte3/plugins/toastr/toastr.min.css">
<link rel = "stylesheet" href = "<?= base_url() . "assets/" ?>app_especifico/css/principal.css">
<link rel = "stylesheet" href = "<?= base_url() . "assets/" ?>adminlte3/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel = "stylesheet" href = "<?= base_url() . "assets/" ?>adminlte3/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
<link rel = "stylesheet" href = "<?= base_url() . "assets/" ?>adminlte3/plugins/datatables-buttons/css/buttons.bootstrap4.min.css">

<link rel="stylesheet" type="text/css" media="screen" href="<?= base_url() . "assets/" ?>css/estilos.php">

<script src="<?= base_url() . "assets/" ?>bootstrap4/js/jquery.min.js"></script>
<style>

    .sidebar-mini.sidebar-collapse .content-wrapper, .sidebar-mini.sidebar-collapse .main-footer, .sidebar-mini.sidebar-collapse .main-header {
        margin-left: 0 !important;
    }

    body:not(.sidebar-mini-md) .content-wrapper, body:not(.sidebar-mini-md) .main-footer, body:not(.sidebar-mini-md) .main-header {
        margin-left: 0 !important;
    }

</style>
