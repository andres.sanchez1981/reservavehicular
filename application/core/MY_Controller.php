<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function marcar($anuncio) {
        $datetime_variable = new DateTime();
        error_log(print_r(date_format($datetime_variable, 'Y-m-d H:i:s'), true) . " " . $anuncio);
    }

    public function mostrarVista($name, $data = array()) {

        $data['claseBody'] = (key_exists('claseBody', $data) ? $data['claseBody'] . " " : "") . "hold-transition";

        if ($this->session->userdata('error') != null) {
            $data['error'] = $this->session->userdata('error');
            $this->session->unset_userdata('error');
        }
        if ($this->session->userdata('exito') != null) {
            $data['exito'] = $this->session->userdata('exito');
            $this->session->unset_userdata('exito');
        }

        $data['body'] = $this->load->view($name, $data, true);

        $data['header'] = $this->load->view('plantilla/header.php', $data, true);
        $data['includes_js'] = $this->load->view('plantilla/js.php', $data, true);
        $data['footer'] = $this->load->view('plantilla/footer.php', $data, true);

        $this->load->view('plantilla/base.php', $data);
    }

    public function obtenerConfiguracion($valor) {
        return $this->service_general->obtenerConfiguracion($valor);
    }
}

?>