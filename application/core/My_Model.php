<?php

class My_Model extends CI_Model {

    public function warning_handler($errno, $errstr, $errfile, $errline, array $errcontex) {
        //error_log("Warning handler");
    }

    function __construct() {
        parent::__construct();
    }

    public function registrarLog($tabla, $datos) {
        $this->ingresar($tabla, $datos, false, true);
    }

    public function retornarConteo($encerar = false) {
        return $this->db->count_all_results('', $encerar);
    }

    public function retornarUno($mostrarSql = false) {
        $resultado = $this->db->get();

        if ($mostrarSql) {
            error_log(print_r($this->db->last_query(), true));
        }

        if ($resultado->num_rows() == 1) {
            return $resultado->row();
        }
        return false;
    }

    public function retornarMuchosSinPaginacion($mostrarSql = false) {
        $resultado = $this->db->get();

        if ($mostrarSql) {
            error_log(print_r($this->db->last_query(), true));
        }
        if ($resultado->num_rows() > 0) {
            return $resultado->result();
        }
        return false;
    }

    public function ingresar($tabla, $datos, $retornar_id = false) {
        $respuesta = $this->db->insert($tabla, $datos);
        if ($respuesta) {
            if ($retornar_id) {
                $last_id = $this->db->insert_id($tabla . '_seq');
                return $last_id;
            }
            return true;
        } else {
            return false;
        }
    }

    public function actualizar($tabla, $datos, $id = "id") {
        if (is_array($id)) {
            $condicion_actualizacion = array();
            foreach ($id as $k => $v) {
                $condicion_actualizacion[$k] = ($v == '-1' ? $datos[$k] : $v);
            }
        } else {
            $condicion_actualizacion = $id . " = " . $datos[$id];
        }

        if ($this->db->update($tabla, $datos, $condicion_actualizacion)) {
            return true;
        } else {
            return false;
        }
    }

    public function retornarSel($arrDatos, $nombre = "nombre", $simple = true) {
        $arr = array();
        if ($arrDatos) {
            if ($simple) {
                foreach ($arrDatos as $dato) {
                    $arr[$dato->id] = $dato->{$nombre};
                }
            } else {
                foreach ($arrDatos as $dato) {
                    $arr[] = array("clave" => $dato->id, "valor" => $dato->{$nombre});
                }
            }
        }
        return $arr;
    }

    public function retonarSelConData($arrDatos, $nombre = "nombre") {
        $arr = array();
        if ($arrDatos) {
            foreach ($arrDatos as $dato) {
                $data_arr = array();
                foreach ($dato as $key => $val) {
                    if (($key != "id") && ($key != $nombre)) {
                        $data_arr[$key] = $val;
                    }
                }
                $arr[] = array("clave" => $dato->id, "valor" => $dato->{$nombre}, "arr_data" => $data_arr);
            }
        }
        return $arr;
    }
}
