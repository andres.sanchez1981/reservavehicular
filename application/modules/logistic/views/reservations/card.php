<div class="col-4 p-1 mb-1" id="driver_<?= $id ?>">
    <div style="background-color:<?= ($estado == 'A' ? "rgba(0, 151, 212, .75);" : "rgba(155, 0, 0, .75);") ?>; " class="rounded">
        <div class="card-header row col-12 pr-1 m-1 ">
            <div class="row col-11">
                <div class="row info-box rcol-12">
                    <div class="col-12 mt-2">
                        <?= $info_driver_surname . " " . $info_driver_name ?>
                    </div>
                    <div class="col-12">
                        License: <?= ($info_driver_license == "Y") ? "Yes" : "No" ?>
                    </div>           
                </div>

                <div class="row info-box col-12">
                    <div class="row col-12">
                        <div class="row m-2">
                            <?= $info_vehicle_brand ?>
                        </div>
                    </div> 
                    <div class="row col-12">
                        <div class="row m-2">
                            <?= $info_vehicle_model ?>
                        </div>
                    </div> 
                    <div class="row col-12">
                        <div class="row m-2">
                            <span><b>Plate: </b><?= $info_vehicle_plate ?></span>
                        </div> 
                    </div>
                    <?php if ($info_vehicle_license_required == "Y") { ?>
                        <div class="row col-12">
                            <div class="row m-2">
                                License Required
                            </div>
                        </div> 
                    <?php } ?>
                </div>
            </div>

            <div class="row col-1 d-flex align-content-start"">
                <button type = "submit" class="btn btn-accion btn-tool" data-id="<?= $id ?>" value="editar"><i class="fas fa-pencil-alt""></i></button>
                <?php if ($estado == ESTADO_ACTIVO) { ?>
                    <button type = "submit" class="btn btn-accion btn-tool" data-id="<?= $id ?>" value="eliminar"><i class="far fa-trash-alt"></i></button>
                    <?php
                } else {
                    echo "";
                }
                ?>
            </div>

        </div>
    </div>
</div>