<div class="wrapper">
    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h4>Trip List</h4>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="<?= base_url() ?>drivers/list">Drivers</a></li>
                            <li class="breadcrumb-item"><a href="<?= base_url() ?>vehicles/list">Vehicles</a></li>
                            <li class="breadcrumb-item">Trips</li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>
        <section class="content">
            <div class="container-fluid">
                <div class="card card-default" id="soloLectura">
                    <?php
                    $filtroActual = array(
                        "texto_busqueda" => $texto_busqueda,
                        "tipo_calendario" => $tipo_calendario,
                        "rango_busqueda" => $rango_busqueda,
                        "uso_calendario" => $uso_calendario,
                        "url_busqueda" => $url_busqueda,
                        "btn_buscar" => '',
                    );
                    echo (filtroBusqueda($url_busqueda, $filtroActual));
                    ?>
                    <div class="card-body text-center">
                        <div class="row small col-12">                            
                            <?php
                            if (!empty($list)) {
                                foreach ($list as $obj) {
                                    echo $this->load->view('logistic/reservations/card.php', $obj);
                                }
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <button type="button" class="float btn-accion" data-id="" value="agregar_orden">
            <i class="fa fa-plus my-float"></i>
        </button>
    </div>
</div>

<div class="modal" id="modalEdicion" tabindex="-1" role="dialog" aria-labelledby="wsanchez" aria-hidden="true">
    <div class="modal-dialog modal-xl modal-dialog-scrollable" role="document">
        <div class="modal-content">

        </div>
    </div>
</div>


<script>
    var obj_actual;
    function recargarPrincipal() {
        $("#btn_buscar").trigger("click");
    }

    function mostrarEdicion(r) {
        if (r.error) {
            mostrarError("Something wrong happend, please contact it crew");
        } else {
            $("#modalEdicion .modal-content").html(r.respuesta);
            $("#modalEdicion").modal("show");

            $('.select_fecha').daterangepicker({
                singleDatePicker: true,
                locale: {
                    format: '<?= FORMATO_FECHA_DATEPICKER_JS ?>'
                },
                minDate: new Date(),
                autoApply: true
            });

            if (r.id) {
                dataToSelect("#vehicle_id", r.sel_vehicles_availables)
                $('#vehicle_id').val(r.obj.vehicle_id);
                $('#vehicle_id').select2().trigger('change');

                dataToSelect("#driver_id", r.sel_drivers_availables)
                $('#driver_id').val(r.obj.driver_id);
                $('#driver_id').select2().trigger('change');
            } else {

                llenarSelect("vehicle_id", '<?= base_url() ?>logistic/reservations/json_vehiclesavailables', {"id": r.id, "date": $('#modalEdicion #date').val()}, function () {
                    $('#modalEdicion #vehicle_id').select2().trigger('change');
                });
            }

            $('#modalEdicion #vehicle_id').on("change", function () {
                llenarSelect("driver_id", '<?= base_url() ?>logistic/reservations/json_driversavailables', {"id": r.id, "vehicle_id": $('#modalEdicion #vehicle_id').val(), "date": $('#modalEdicion #date').val()}, false);
            });
            $('#modalEdicion #date').on('apply.daterangepicker', function (e, picker) {
                llenarSelect("vehicle_id", '<?= base_url() ?>logistic/reservations/json_vehiclesavailables', {"id": r.id, "date": $('#modalEdicion #date').val()}, function () {
                    $('#modalEdicion #vehicle_id').select2().trigger('change');
                });
            });


        }
    }
    function mostrarEliminacion(r) {
        if (r.error) {
            mostrarError(r.respuesta);
        } else {
            mostrarExito(r.respuesta);
            recargarPrincipal();
        }
    }

    function loadReservacion() {
        llamadaAjax(false, '<?= base_url() ?>logistic/reservations/json_get', {"id": obj_actual}, mostrarEdicion);
    }

    $(document).on('click', '.btn-accion', function () {
        unsoloclick('.btn-accion');
        switch ($(this).val()) {
            case "agregar_orden":
                llamadaAjax(false, '<?= base_url() ?>logistic/reservations/json_new', false, mostrarEdicion);
                break;
            case "eliminar":
                swal_modal('Are you sure?',
                        'Yes',
                        'No',
                        '<?= base_url() ?>logistic/reservations/json_delete',
                        {"id": $(this).data('id')},
                        mostrarEliminacion);
                break;
            case "editar":
                obj_actual = $(this).data('id');
                loadReservacion();
                break;

            default:
                alert('undefined action to ' + $(this).val());
                break;
        }
    });

</script>
