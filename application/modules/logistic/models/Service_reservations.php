<?php

class Service_reservations extends My_Model {

    public function list($filtro) {
        $this->db->select("t.*, d.surname as info_driver_surname, d.name as info_driver_name, d.license as info_driver_license, v.brand as info_vehicle_brand, v.model as info_vehicle_model, v.plate as info_vehicle_plate, v.license_required as info_vehicle_license_required");
        $this->db->from('logistic.trip t');
        $this->db->join('resources.driver d', 't.driver_id = d.id', 'left');
        $this->db->join('resources.vehicle v', 't.vehicle_id = v.id', 'left');

        if (!empty($filtro['texto_busqueda'])) {
            $this->db->where("(v.brand LIKE '%" . $filtro['texto_busqueda'] . "%' OR v.model LIKE '%" . $filtro['texto_busqueda'] . "%' OR v.plate LIKE '%" . $filtro['texto_busqueda'] . "%' "
                    . "OR d.name LIKE '%" . $filtro['texto_busqueda'] . "%' OR d.surname LIKE '%" . $filtro['texto_busqueda'] . "%' )");
        }

        if (!empty($filtro['tipo_calendario'])) {
            $this->agregarRangoFechas($filtro);
        }
        $this->db->order_by('t.date ASC');
        return $this->retornarMuchosSinPaginacion();
    }

    private function agregarRangoFechas($filtro) {
        $arrRango = explode(" - ", $filtro['rango_busqueda']);
        if (sizeof($arrRango) != 2) {
            return false;
        }
        $fechaIni = date_format(DateTime::createFromFormat(FORMATO_FECHA, convertirFechaBD($arrRango[0])), 'Y-m-d 00:00:00');
        $fechaFin = date_format(DateTime::createFromFormat(FORMATO_FECHA, convertirFechaBD($arrRango[1])), 'Y-m-d 23:59:59');

        switch ($filtro['tipo_calendario']) {
            case 0://Registration
                $arrSelect = array('t.date >= ' => $fechaIni, 't.date <= ' => $fechaFin);
                break;
            default:
                $arrSelect = array('t.date >= ' => $fechaIni, 't.date <= ' => $fechaFin);
                break;
        }
        $this->db->where($arrSelect);
    }

    public function getOne($id) {
        $this->db->select("t.*, v.id as info_vehicle_id, d.id as info_driver_id");
        $this->db->from('logistic.trip t');
        $this->db->join("resources.vehicle v", "t.vehicle_id = v.id");
        $this->db->join("resources.driver d", "t.driver_id = d.id");
        $this->db->where("t.id", $id);
        return $this->retornarUno(true);
    }

    public function getNew() {
        return (object) [
                    'vehicle_id' => '',
                    'driver_id' => '',
                    'date' => fechaActual(FORMATO_FECHA),
                    'estado' => ESTADO_ACTIVO,
        ];
    }

    public function persistenciaObj($arr) {
        $respuesta = "";

        if (!empty($arr['id'])) {
            $id = $this->update($arr);
            $respuesta = (!$id ? 'There was a problem while updating this item' : 'Updated');
        } else {
            $id = $this->create($arr);
            $respuesta = (!$id ? 'There was a problem creating this item' : 'Created');
        }

        return array($respuesta, $id);
    }

    public function create($obj) {
        return $this->ingresar("logistic.trip", $obj, true);
    }

    public function update($obj) {
        return $this->actualizar("logistic.trip", $obj, "id");
    }
    
    public function selVehiclesAvailables($date, $trip_id) {
        $this->db->select("v.*, v.brand || ' | ' || v.model || CASE WHEN v.license_required = 'Y' THEN ' (License Required)' ELSE '' END as data_vehiculo");
        $this->db->from('resources.vehicle v');
        $this->db->join("logistic.trip t", "t.vehicle_id = v.id AND t.estado = '".ESTADO_ACTIVO."' AND t.date = '".$date."' ", 'left');
        $this->db->where('v.estado', ESTADO_ACTIVO);
        $this->db->where("(t.id IS NULL ". ($trip_id? 'OR t.id = '.$trip_id:'') .")");
        $this->db->order_by("v.brand ASC", "v.model ASC", "v.license_required ASC");
        return $this->retornarSel($this->retornarMuchosSinPaginacion(true), "data_vehiculo", false);
    }
    
    public function selDriversAvailables($date, $vehicle_id, $trip_id) {
        if (empty($vehicle_id)){
            return array();
        }
        $this->db->select("d.*, d.surname || ' | ' || d.name || CASE WHEN d.license = 'Y' THEN ' (License)' ELSE '' END as data_driver");
        $this->db->from('resources.v_vehicle_driver_combination vdc');        
        $this->db->join("logistic.trip t", "t.driver_id = vdc.driver_id AND t.estado = '".ESTADO_ACTIVO."' AND t.date = '".$date."' ", 'left');
        $this->db->join("resources.driver d", "vdc.driver_id = d.id");
        $this->db->where('vdc.vehicle_id', $vehicle_id);
        $this->db->where("(t.id IS NULL ". ($trip_id? 'OR t.id = '.$trip_id:'') .")");
        $this->db->where('vdc.valido', 1);
        $this->db->order_by("d.surname ASC", "d.name ASC");
        return $this->retornarSel($this->retornarMuchosSinPaginacion(), "data_driver", false);
    }
}
