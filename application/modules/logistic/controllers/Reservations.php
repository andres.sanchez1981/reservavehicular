<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Reservations extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model("logistic/service_reservations");
    }

    public function index() {
        $this->list();
    }

    public function list() {
        $data = array();
        $data['texto_busqueda'] = "";
        $data['rango_busqueda'] = '';
        $data['tipo_calendario'] = 0;
        $data['uso_calendario'] = 0;
        if ($this->input->post('btn_buscar') != null) {
            $data['texto_busqueda'] = $this->input->post('texto_busqueda');
            $data['rango_busqueda'] = $this->input->post('rango_busqueda');
            $data['tipo_calendario'] = $this->input->post('tipo_calendario');

            $data['list'] = $this->service_reservations->list($data);
        }

        $data['url_busqueda'] = "logistic/reservations/list";
        $this->mostrarVista('logistic/reservations/list.php', $data);
    }

    public function json_new() {
        $this->json_get();
    }

    public function json_get() {
        $id = false;
        if (!empty($this->input->post('id'))) {
            $id = $this->input->post('id');
            $data['operacion'] = '<i class="fa fa-pencil-alt my-float"></i> Update';
            $data['obj'] = $this->service_reservations->getOne($id);
            $data['sel_vehicles_availables'] = $this->service_reservations->selVehiclesAvailables($data['obj']->date, $data['obj']->id);
            $data['sel_drivers_availables'] = $this->service_reservations->selDriversAvailables($data['obj']->date, $data['obj']->vehicle_id, $data['obj']->id);
        } else {
            $data['operacion'] = '<i class="fa fa-plus my-float"></i> Creation';
            $data['obj'] = $this->service_reservations->getNew();
        }

        $obj_det = $this->load->view('logistic/reservations/obj.php', $data, true);
        $respuesta = array("error" => (!$data['obj'] ? true : false), "respuesta" => $obj_det, "obj" => $data['obj'], "sel_vehicles_availables" => $data['sel_vehicles_availables'], "sel_drivers_availables" => $data['sel_drivers_availables']);
        if ($id) {
            $respuesta['id'] = $id;
        }
        header('Content-Type: application/json');
        echo json_encode($respuesta);
    }

    public function json_save() {
        $actualizacion = false;
        if (!empty($this->input->post('id'))) {
            $id = $this->input->post('id');
            $obj = $this->service_reservations->getOne($id);
        } else {
            $id = false;
            $obj = $this->service_reservations->getNew();
        }
        $arr = array();
        if ($obj) {
            foreach ($obj as $field => $value) {
                if (!(strpos(strtoupper($field), 'CREACION_') === 0 || strpos(strtoupper($field), 'ACTUALIZACION_') === 0 || strpos(strtoupper($field), 'INFO_') === 0)) {
                    $arr[$field] = $this->input->post($field);
                }
            }
            List($respuesta, $obj) = $this->service_reservations->persistenciaObj($arr);
        } else {
            $respuesta = 'Id not found';
        }

        $respuesta = array("error" => !$obj, "respuesta" => $respuesta);

        header('Content-Type: application/json');
        echo json_encode($respuesta);
    }

    public function json_delete() {
        $id = $this->input->post('id');
        $actualizacion = $this->service_reservations->update(array("id" => $id, "estado" => ESTADO_INACTIVO), true);

        header('Content-Type: application/json');
        echo json_encode(array("error" => !$actualizacion, "respuesta" => (!$actualizacion ? 'Something wrong happend' : 'Inactivated')));
    }

    public function json_vehiclesavailables() {
        $date = $this->input->post('date');
        $id = $this->input->post('id');
        $arr = $this->service_reservations->selVehiclesAvailables($date, $id);
        header('Content-Type: application/json');
        echo json_encode($arr);
    }

    public function json_driversavailables() {
        $date = $this->input->post('date');
        $id = $this->input->post('id');
        $vehicle_id = $this->input->post('vehicle_id');
        $arr = $this->service_reservations->selDriversAvailables($date, $vehicle_id, $id);
        header('Content-Type: application/json');
        echo json_encode($arr);
    }
}
