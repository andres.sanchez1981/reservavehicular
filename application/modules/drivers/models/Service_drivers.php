<?php

class Service_drivers extends My_Model {

    public function list($filtro) {
        $this->db->select("d.*");
        $this->db->from('resources.driver d');

        if (!empty($filtro['texto_busqueda'])) {
            $this->db->where("(d.surname LIKE '%" . $filtro['texto_busqueda'] . "%' OR d.name LIKE '%" . $filtro['texto_busqueda'] . "%' )");
        }
        $this->db->order_by('d.surname ASC', 'd.name ASC');
        return $this->retornarMuchosSinPaginacion();
    }

    public function getOne($id) {
        $this->db->from('resources.driver d');
        $this->db->where("d.id", $id);
        return $this->retornarUno();
    }

    public function getNew() {
        return (object) [
                    'name' => '',
                    'surname' => '',
                    'license' => '',
                    'estado' => ESTADO_ACTIVO,
        ];
    }

    public function nameDriverExist($obj) {
        $this->db->from('resources.driver d');
        if (array_key_exists("id", $obj)) {
            $this->db->where('id <>', $obj['id']);
        }
        if (array_key_exists("name", $obj) && array_key_exists("surname", $obj)) {
            $this->db->where('name', $obj['name']);
            $this->db->where('surname', $obj['surname']);
        }
        return $this->retornarConteo(true);
    }

    public function persistenciaObj($arr) {
        $respuesta = "";
        $existeObj = $this->nameDriverExist($arr);
        if ($existeObj) {
            return array('Name and surname already registered', false);
        }

        if (!empty($arr['id'])) {
            $id = $this->update($arr);
            $respuesta = (!$id ? 'There was a problem while updating this item' : 'Updated');
        } else {
            $id = $this->create($arr);
            $respuesta = (!$id ? 'There was a problem creating this item' : 'Created');
        }

        return array($respuesta, $id);
    }

    public function create($obj) {
        return $this->ingresar("resources.driver", $obj, true);
    }

    public function update($obj) {
        return $this->actualizar("resources.driver", $obj, "id");
    }
}
