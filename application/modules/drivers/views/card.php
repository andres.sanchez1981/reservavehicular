<div class="col-lg-4 col-md-6 col-sm-12 p-1 mb-1" id="driver_<?= $id ?>">
    <div class="card rounded" style="background-color: #A3EBF0">
        <div class="row no-gutters">
            <!-- Lado Izquierdo con Imagen -->
            <div class="col-md-4">
                <img src="images/user.jpg" class="card-img" alt="Imagen del Conductor">
            </div>
            <!-- Lado Derecho con Información y Botones -->
            <div class="col-md-8">
                <div class="card-header bg-primary text-white">
                    <?= $surname . " " . $name ?>
                </div>
                <div class="card-body">
                    <?php if ($estado == ESTADO_INACTIVO) { ?>
                        <div class="text-danger mb-2">
                            Estado: Inactivo
                        </div>
                    <?php } ?>
                    <div>
                        License: <?= ($license == "Y") ? "Yes" : "No" ?>
                    </div>
                </div>
                <div class="card-footer text-md-right d-flex justify-content-center">
                    <button type="submit" class="btn btn-accion btn-tool" data-id="<?= $id ?>" value="editar" style="background-color: #2ecc71; color: #fff;">
                        <i class="fas fa-pencil-alt"></i> Editar
                    </button>
                    <?php if ($estado == ESTADO_ACTIVO) { ?>
                        <button type="submit" class="btn btn-accion btn-tool" data-id="<?= $id ?>" value="eliminar" style="background-color: #e74c3c; color: #fff;">
                            <i class="far fa-trash-alt"></i> Eliminar
                        </button>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>
