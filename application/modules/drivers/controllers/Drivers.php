<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Drivers extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model("drivers/service_drivers");
    }
    
    public function index() {
        $this->list();
    }

    public function list() {
        $data = array();
        $data['texto_busqueda'] = "";
        if ($this->input->post('btn_buscar') != null) {
            $data['texto_busqueda'] = $this->input->post('texto_busqueda');

            $data['list'] = $this->service_drivers->list($data);
        }

        $data['url_busqueda'] = "drivers/list";
        $this->mostrarVista('drivers/list.php', $data);
    }

    public function json_new() {
        $this->json_get();
    }

    public function json_get() {
        if (!empty($this->input->post('id'))) {
            $id = $this->input->post('id');
            $data['operacion'] = '<i class="fa fa-pencil-alt my-float"></i> Update';
            $data['obj'] = $this->service_drivers->getOne($id);
        } else {
            $data['operacion'] = '<i class="fa fa-plus my-float"></i> Creation';
            $data['obj'] = $this->service_drivers->getNew();
        }

        $obj_det = $this->load->view('obj.php', $data, true);

        $respuesta = array("error" => (!$data['obj'] ? true : false), "respuesta" => $obj_det);

        header('Content-Type: application/json');
        echo json_encode($respuesta);
    }

    public function json_save() {
        $actualizacion = false;
        if (!empty($this->input->post('id'))) {
            $id = $this->input->post('id');
            $obj = $this->service_drivers->getOne($id);
        } else {
            $id = false;
            $obj = $this->service_drivers->getNew();
        }
        $arr = array();
        if ($obj) {
            foreach ($obj as $field => $value) {
                if (!(strpos(strtoupper($field), 'CREACION_') === 0 || strpos(strtoupper($field), 'ACTUALIZACION_') === 0 || strpos(strtoupper($field), 'INFO_') === 0)) {
                    $arr[$field] = $this->input->post($field);
                }
            }
            List($respuesta, $obj) = $this->service_drivers->persistenciaObj($arr);
        } else {
            $respuesta = 'Id not found';
        }

        $respuesta = array("error" => !$obj, "respuesta" => $respuesta);

        header('Content-Type: application/json');
        echo json_encode($respuesta);
    }

    public function json_delete() {
        $id = $this->input->post('id');
        $actualizacion = $this->service_drivers->update(array("id" => $id, "estado" => ESTADO_INACTIVO), true);

        header('Content-Type: application/json');
        echo json_encode(array("error" => !$actualizacion, "respuesta" => (!$actualizacion ? 'Something wrong happend' : 'Inactivated')));
    }
}
