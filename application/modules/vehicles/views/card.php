<div class="col-6 p-1 mb-1" id="vehicle_<?= $id ?>">
    <div style="background-color:<?= ($estado == 'A' ? "rgba(0, 151, 212, .75);" : "rgba(155, 0, 0, .75);") ?>; " class="rounded">
        <div class="card-header row col-12 pr-1 m-1 ">
            <div class="row col-6">
                <span class="info-box row">
                    <div class="col-12 mt-2">
                        <?= $plate ?>
                    </div>
                    <?php if ($estado == ESTADO_INACTIVO) { ?>
                        <div class="col-12">
                            Inactive
                        </div>
                    <?php } ?>
                </span>
            </div>
            <div class="row col-5">
                <div class="row col-12">
                    <div class="row m-2">
                        <?= $brand ?>
                    </div>
                </div> 
                <div class="row col-12">
                    <div class="row m-2">
                        <?= $model ?>
                    </div>
                </div> 
                <div class="row col-12">
                    <div class="row m-2"><span><b>Plate: </b><?= $plate ?></span>
                    </div>
                </div> 
                <?php if ($license_required == "Y") { ?>
                    <div class="row col-12">
                        <div class="row m-2">
                            License Required
                        </div>
                    </div> 
                <?php } ?>
            </div>
            <div class="row col-1 d-flex align-content-start"">
                <button type = "submit" class="btn btn-accion btn-tool" data-id="<?= $id ?>" value="editar"><i class="fas fa-pencil-alt""></i></button>
                <?php if ($estado == ESTADO_ACTIVO) { ?>
                    <button type = "submit" class="btn btn-accion btn-tool" data-id="<?= $id ?>" value="eliminar"><i class="far fa-trash-alt"></i></button>
                <?php } ?>
            </div>
        </div>
    </div>
</div>