<div class="wrapper">
    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h4>Fleet</h4>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="<?=base_url()?>drivers/list">Drivers</a></li>
                            <li class="breadcrumb-item">Vehicles</li>
                            <li class="breadcrumb-item"><a href="<?=base_url()?>logistic/reservations/list">Trips</a></li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>
        <section class="content">
            <div class="container-fluid">
                <div class="card card-default" id="soloLectura">
                    <?php
                    $filtroActual = array(
                        "texto_busqueda" => $texto_busqueda,
                        "url_busqueda" => $url_busqueda,
                        "btn_buscar" => '',
                    );
                    echo (filtroBusqueda($url_busqueda, $filtroActual));
                    ?>
                    <div class="card-body text-center">
                        <div class="row small col-12">                            
                            <?php
                            if (!empty($list)) {
                                foreach ($list as $obj) {
                                    echo $this->load->view('vehicles/card.php', $obj);
                                }
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <button type="button" class="float btn-accion" data-id="" value="agregar_orden">
            <i class="fa fa-plus my-float"></i>
        </button>
    </div>
</div>

<div class="modal" id="modalEdicion" tabindex="-1" role="dialog" aria-labelledby="wsanchez" aria-hidden="true">
    <div class="modal-dialog modal-xl modal-dialog-scrollable" role="document">
        <div class="modal-content">

        </div>
    </div>
</div>


<script>
    var obj_actual;
    function recargarPrincipal() {
        $("#btn_buscar").trigger("click");
    }

    function mostrarEdicion(r) {
        if (r.error) {
            mostrarError("Something wrong happend, please contact it crew");
        } else {
            $("#modalEdicion .modal-content").html(r.respuesta);
            $("#modalEdicion").modal("show");
        }
    }
    function mostrarEliminacion(r) {
        if (r.error) {
            mostrarError(r.respuesta);
        } else {
            mostrarExito(r.respuesta);
            recargarPrincipal();
        }
    }

    function loadVehicle() {
        llamadaAjax(false, '<?= base_url() ?>vehicles/json_get', {"id": obj_actual}, mostrarEdicion);
    }

    $(document).on('click', '.btn-accion', function () {
        unsoloclick('.btn-accion');
        switch ($(this).val()) {
            case "agregar_orden":
                llamadaAjax(false, '<?= base_url() ?>vehicles/json_new', false, mostrarEdicion);
                break;
            case "eliminar":
                swal_modal('Are you sure?',
                        'Yes',
                        'No',
                        '<?= base_url() ?>vehicles/json_delete',
                        {"id": $(this).data('id')},
                        mostrarEliminacion);
                break;
            case "editar":
                obj_actual = $(this).data('id');
                loadVehicle();
                break;

            default:
                alert('undefined action to ' + $(this).val());
                break;
        }
    });
</script>
