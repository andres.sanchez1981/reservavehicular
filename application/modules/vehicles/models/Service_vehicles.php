<?php

class Service_vehicles extends My_Model {

    public function list($filtro) {
        $this->db->select("v.*");
        $this->db->from('resources.vehicle v');

        if (!empty($filtro['texto_busqueda'])) {
            $this->db->where("(v.brand LIKE '%" . $filtro['texto_busqueda'] . "%' OR v.model LIKE '%" . $filtro['texto_busqueda'] . "%' OR v.plate LIKE '%" . $filtro['texto_busqueda'] . "%' )");
        }
        $this->db->order_by('v.plate', 'ASC');
        return $this->retornarMuchosSinPaginacion();
    }

    public function getOne($id) {
        $this->db->from('resources.vehicle v');
        $this->db->where("v.id", $id);
        return $this->retornarUno();
    }

    public function getNew() {
        return (object) [
                    'brand' => '',
                    'model' => '',
                    'plate' => '',
                    'license_required' => '',
                    'estado' => ESTADO_ACTIVO,
        ];
    }

    public function plateActive($obj) {
        $this->db->from('resources.vehicle v');
        if (array_key_exists("id", $obj)) {
            $this->db->where('id <>', $obj['id']);
        }
        if (array_key_exists("plate", $obj)) {
            $this->db->where('plate', $obj['plate']);
        }
        return $this->retornarConteo(true);
    }

    public function persistenciaObj($arr) {
        $respuesta = "";
        $existeObj = $this->plateActive($arr);
        if ($existeObj) {
            return array('Plate already registered', false);
        }

        if (!empty($arr['id'])) {
            $id = $this->update($arr);
            $respuesta = (!$id ? 'There was a problem while updating this item' : 'Updated');
        } else {
            $id = $this->create($arr);
            $respuesta = (!$id ? 'There was a problem creating this item' : 'Created');
        }

        return array($respuesta, $id);
    }

    public function create($obj) {
        return $this->ingresar("resources.vehicle", $obj, true);
    }

    public function update($obj) {
        return $this->actualizar("resources.vehicle", $obj, "id");
    }
}
