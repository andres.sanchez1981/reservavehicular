<?php
define("ESPACIADO_ELEMENTOS_DOBLE", "col-6");
define("ESPACIADO_ELEMENTOS_SIMPLE", "col-2");

function filtroBusqueda($url_busqueda, $filtro) {
    $respuesta = form_open(base_url() . $filtro['url_busqueda'], array("id" => "form_busqueda"));
    $respuesta .= '<div class="card-body card-busqueda">';
    $respuesta .= '<div class="form-row small" style="font-size: 0.8rem">';
    foreach ($filtro as $key => $value) {
        switch (strval($key)) {
            case 'btn_buscar':
                $respuesta .= btnBuscar();
                break;
            case 'rango_busqueda':
                $respuesta .= selFechasRango($filtro['tipo_calendario'], $filtro['rango_busqueda'], $filtro['uso_calendario']);
                break;
            case 'texto_busqueda':
                $respuesta .= texto_busqueda($value);
                break;
            default:
                break;
        }
    }
    $respuesta .= '</div>';
    $respuesta .= '</div>';
    $respuesta .= form_close();
    return $respuesta;
}

function btnBuscar() {
    ob_start();
    ?>
    <div class="<?= ESPACIADO_ELEMENTOS_SIMPLE ?>">
        <button type="submit" name="btn_buscar" id="btn_buscar" class="btn btn-primary btn-block"><i class="fas fa-search"></i></button>
    </div>
    <?php
    $output = ob_get_contents();
    ob_end_clean();
    return $output;
}

function texto_busqueda($texto_busqueda) {
    return texto('texto_busqueda', 'Contenga', $texto_busqueda);
}

function texto($id, $label, $valor_actual) {
    ob_start();
    ?>
    <div class="<?= ESPACIADO_ELEMENTOS_DOBLE ?>">
        <div class="input-group mb-0">
            <div class="input-group-prepend ajustar_altura">
                <span class="input-group-text" id="basic-<?= $id ?>"><?= $label ?></span>
            </div>
            <input type="text" class="form-control" id="<?= $id ?>" name="<?= $id ?>" aria-describedby="basic-<?= $id ?>" placeholder="<?= $label ?>" value="<?= $valor_actual ?>">
        </div>
    </div>
    <?php
    $output = ob_get_contents();
    ob_end_clean();
    return $output;
}

function item_input($arr) {
    $input = false;
    $dd_js = array();
    if (key_exists('js', $arr)) {
        $dd_js = $arr['js'];
    }
    $dd_js['id'] = $arr['id'];
    $dd_js['class'] = key_exists('clase', $arr) ? $arr['clase'] : '';
    $dd_js['maxlength'] = key_exists('maxlength', $arr) ? $arr['maxlength'] : '';
    $dd_js['aria-describedby'] = "basic-" . $dd_js['id'];

    switch (strtolower($arr['tipo'])) {
        case 'input':
            $input = form_input($arr['name'], $arr['value'], $dd_js);
            break;
        case 'email':
            $dd_js['class'] .= " soloEmail";
            $dd_js['type'] = "email";
            $input = form_input($arr['name'], $arr['value'], $dd_js);
            break;
        case 'number':
            $dd_js['class'] .= " soloNumeros";
            $dd_js['type'] = "number";
            $input = form_input($arr['name'], $arr['value'], $dd_js);
            $input = str_replace('type="text"', 'type="number"', $input); //wsanchez implementacion pendiente
            break;
        case 'hidden':
            echo form_hidden($arr['name'], $arr['value']);
            break;
        case 'textarea':
            $dd_js['style'] = "width:90%";
            $input = form_textarea($arr['name'], $arr['value'], $dd_js);
            break;
        case 'select':
            $dd_js['class'] .= " form-control select2";
            $input = form_dropdown($arr['name'], $arr['sel'], $arr['value'], $dd_js);
            break;
        case 'select_multiple':
            $dd_js['class'] .= " form-control select2";
            $input = form_multiselect($arr['name'] . '[]', $arr['sel'], $arr['value'], $dd_js);
            break;
        case 'password':
            $dd_js['type'] = "password";
            $input = form_password($arr['name'], $arr['value'], $dd_js);
            $input = '<p>
                        <input type="password" name="' . $arr['name'] . '" class="' . print_r($dd_js['class'], true) . '"/>
                        <span class="togglePassword">
                            <i class="fa-solid fa-eye-slash ml-1" style="width:50px"></i>
                        </span>
                    </p>';
            break;
        case 'date':
            $dd_js['type'] = "date";
            $input = form_date($arr['name'], $arr['value'], $dd_js);
            break;
        case 'label':
            $dd_js['readonly'] = 'true';
            $input = form_input($arr['name'], $arr['value'], $dd_js);
            break;
        case 'file':
            $dd_js['type'] = 'file';
            $arr_input = array('name' => $arr['name'], 'accept' => 'application/pdf');
            $input = form_upload($arr_input, '', $dd_js);
            break;
        case 'files':
            $dd_js['type'] = 'file';
            $arr_input = array('name' => $arr['name'] . '[]', 'accept' => 'application/pdf', 'multiple' => '');
            $input = form_upload($arr_input, '', $dd_js);
            break;
        case 'select_disable':
            $dd_js['disabled'] = true;
            $input = form_dropdown($arr['name'], $arr['sel'], $arr['value'], $dd_js);
            break;
        case 'fecha_unica':
            $input = inputFecha('Seleccionada', 'date', $arr['value']);
            break;
        default:
            break;
    }
    return $input;
}

function item_formulario_vertical($arr, $num_items = false) {
    if (array_key_exists('clase', $arr)) {
        $arr['clase'] .= ' col-12 col-md-8 col-lg-8';
    } else {
        $arr['clase'] = 'col-12 col-md-8 col-lg-8';
    }
    $arr['dis'] = 'enabled';
    $input = item_input($arr);
    ob_start();
    if ($input) {
        ?>
        <div class = "form-group col-12 col-lg-6">
            <div class="input-group mb-0">
                <div class="input-group-prepend ajustar_altura col-12 col-md-4 col-lg-4 justify-content-end">
                    <span class="input-group-text-modal-edicion" id="basic-<?= $arr['name'] ?>"><?php
                        if (key_exists('label', $arr)) {
                            $etiqueta = $arr['label'];
                        } else {
                            $etiqueta = "";
                            $arr = explode("_", $arr['name']);
                            foreach ($arr as $a) {
                                $etiqueta .= ucfirst($a) . " ";
                            }
                            if (empty($etiqueta)) {
                                $etiqueta = ucfirst($arr['name']);
                            }
                        }
                        echo $etiqueta;
                        ?>
                    </span>
                </div>
                <?= $input; ?>
            </div>
        </div>
        <?php
    }
    $output = ob_get_contents();
    ob_end_clean();
    return $output;
}

function selFechasRango($tipo_calendario, $rango, $uso_calendario) {
    ob_start();

    switch ($uso_calendario) {
        case 0:
            $selTipoCalendario = array(0 => "Trip");
            break;
        default:
            $selTipoCalendario = array(10 => "Unknown");
            break;
    }

    $dd_js = array(
        "id" => "tipo_calendario",
        "class" => "form-control select2",
    );
    ?>
    <div class="<?= ESPACIADO_ELEMENTOS_DOBLE ?>">
        <div class="input-group">
            <div class="input-group-prepend ajustar_altura">
                <span class="input-group-text">
                    <i class="fas fa-calendar-alt"></i>
                </span>
                <?= form_dropdown($dd_js["id"], $selTipoCalendario, $tipo_calendario, $dd_js) ?>
            </div>

            <input type="text" class="form-control float-right fecha_rango w-100" id="rango_busqueda" name="rango_busqueda" value="<?= $rango ?>">
        </div>
    </div>
    <?php
    $output = ob_get_contents();
    ob_end_clean();
    return $output;
}

function fechaActual($formato = false) {
    $datetime_variable = new DateTime();
    $datetime_formatted = date_format($datetime_variable, ($formato ? $formato : FORMATO_FECHA_COMPLETO));
    return $datetime_formatted;
}

function inputFecha($label, $id, $fechaSeleccionada) {
    ob_start();
    ?>
    <input type="text" class="form-control col-7 col-sm-8 col-md-12 col-xl-8 select_fecha <?= $id ?>" id="<?= $id ?>" name="<?= $id ?>" aria-describedby="basic-<?= $id ?>" value="<?= $fechaSeleccionada ?>">

    <?php
    $output = ob_get_contents();
    ob_end_clean();
    return $output;
}
?>