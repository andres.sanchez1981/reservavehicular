<?php
// TCPDF FONT FILE DESCRIPTION
$type='TrueTypeUnicode';
$name='NotoSansOldPersian';
$up=-75;
$ut=50;
$dw=600;
$diff='';
$originalsize=12420;
$enc='';
$file='notosansoldpersian.z';
$ctg='notosansoldpersian.ctg.z';
$desc=array('Flags'=>32,'FontBBox'=>'[94 -147 1615 780]','ItalicAngle'=>0,'Ascent'=>1069,'Descent'=>-293,'Leading'=>0,'CapHeight'=>714,'XHeight'=>714,'StemV'=>34,'StemH'=>15,'AvgWidth'=>1098,'MaxWidth'=>1724,'MissingWidth'=>600);
$cw=array(0=>0,13=>510,32=>260,160=>260,65279=>0);
// --- EOF ---
